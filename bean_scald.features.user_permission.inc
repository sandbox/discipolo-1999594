<?php
/**
 * @file
 * bean_scald.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bean_scald_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create any scald_bean bean'.
  $permissions['create any scald_bean bean'] = array(
    'name' => 'create any scald_bean bean',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site manager' => 'site manager',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'delete any scald_bean bean'.
  $permissions['delete any scald_bean bean'] = array(
    'name' => 'delete any scald_bean bean',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site manager' => 'site manager',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit any scald_bean bean'.
  $permissions['edit any scald_bean bean'] = array(
    'name' => 'edit any scald_bean bean',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site manager' => 'site manager',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view any scald_bean bean'.
  $permissions['view any scald_bean bean'] = array(
    'name' => 'view any scald_bean bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
      'site manager' => 'site manager',
    ),
    'module' => 'bean',
  );

  return $permissions;
}

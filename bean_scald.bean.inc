<?php
/**
 * @file
 * bean_scald.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function bean_scald_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'scald_bean';
  $bean_type->label = 'scald bean';
  $bean_type->options = '';
  $bean_type->description = 'a block for embedding with scald';
  $export['scald_bean'] = $bean_type;

  return $export;
}

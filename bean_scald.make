core = 7.x
api = 2


projects[bean][version] = 1.2
projects[bean][subdir] = contrib

projects[ctools][version] = 1.3
projects[ctools][subdir] = contrib

projects[features][download][type] = "git"
projects[features][download][url] = "http://git.drupal.org/project/features.git"
projects[features][download][branch] = "7.x-2.x"
projects[features][subdir] = contrib


projects[scald][download][type] = git
projects[scald][download][url] = http://git.drupal.org/project/scald.git
projects[scald][download][branch] = 7.x-1.x
projects[scald][patch][2007740] = https://drupal.org/files/scald-dnd-block-2007740-8.patch
projects[scald][patch][2063785] = https://drupal.org/files/youtube-dimension-player.patch
projects[scald][subdir] = "contrib"

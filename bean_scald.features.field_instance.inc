<?php
/**
 * @file
 * bean_scald.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function bean_scald_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'bean-scald_bean-field_embedded_code'
  $field_instances['bean-scald_bean-field_embedded_code'] = array(
    'bundle' => 'scald_bean',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'bean',
    'field_name' => 'field_embedded_code',
    'label' => 'embedded code',
    'required' => 0,
    'settings' => array(
      'context' => 'debug',
      'display_summary' => 1,
      'dnd_enabled' => 1,
      'mee_enabled' => 1,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 10,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'bean-scald_bean-field_scald_file'
  $field_instances['bean-scald_bean-field_scald_file'] = array(
    'bundle' => 'scald_bean',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'atom_reference',
        'settings' => array(),
        'type' => 'sidebar',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bean',
    'field_name' => 'field_scald_file',
    'label' => 'embedded file',
    'required' => 0,
    'settings' => array(
      'referencable_types' => array(
        'audio' => 'audio',
        'embed' => 'embed',
        'gallery' => 'gallery',
        'image' => 'image',
        'video' => 'video',
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'atom_reference',
      'settings' => array(),
      'type' => 'atom_reference_textfield',
      'weight' => 11,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('embedded code');
  t('embedded file');

  return $field_instances;
}
